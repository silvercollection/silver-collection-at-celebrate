Silver Collection at Celebrate is the newest, one-of-a-kind, resort style apartment community, located off of Celebrate Virginia Parkway. Our residents experience concierge-style services that offer everything you would expect in a five-star resort, including an on-site activities planner.

Address: 2530 Celebrate Virginia Parkway, Fredericksburg, VA 22406, USA
Phone: 540-684-3468
